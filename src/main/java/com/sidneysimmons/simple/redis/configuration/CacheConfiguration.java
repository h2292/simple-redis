package com.sidneysimmons.simple.redis.configuration;

import java.time.Duration;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair;

import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;

/**
 * Cache configuration.
 */
@Configuration
@EnableCaching
public class CacheConfiguration implements CachingConfigurer {

    @Autowired
    private Environment environment;

    public static final String SHORT_LIVED_CACHE = "shortLivedCache";
    public static final String LONG_LIVED_CACHE = "longLivedCache";

    /**
     * Configure the redis connection factory.
     * 
     * @return the factory
     */
    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        // Create the redis configuration
        RedisStandaloneConfiguration redisConfiguration = new RedisStandaloneConfiguration("simple-redis-redis", 6379);

        // Create the client configuration
        LettuceClientConfiguration clientConfiguration = LettuceClientConfiguration.builder()
                .commandTimeout(Duration.ofSeconds(5)).build();

        // Return the factory
        return new LettuceConnectionFactory(redisConfiguration, clientConfiguration);
    }

    /**
     * Customize the redis cache manager builder with specific cache regions.
     * 
     * @return the customizer
     */
    @Bean
    public RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() {
        // Get the name of the application to prefix/namespace the cache keys
        String applicationName = environment.getProperty("spring.application.name");
        if (applicationName == null) {
            throw new IllegalStateException("Application name is required for prefixing the redis keys.");
        }
        applicationName += "-";

        // Create a JSON serializer for the cache values
        SerializationPair<Object> jsonSerializationPair = createJsonSerializer();

        // Short lived cache
        RedisCacheConfiguration shortLivedCache = RedisCacheConfiguration.defaultCacheConfig()
                .serializeValuesWith(jsonSerializationPair).entryTtl(Duration.ofMinutes(1))
                .prefixCacheNameWith(applicationName);

        // Long lived cache
        RedisCacheConfiguration longLivedCache = RedisCacheConfiguration.defaultCacheConfig()
                .serializeValuesWith(jsonSerializationPair).entryTtl(Duration.ofMinutes(10))
                .prefixCacheNameWith(applicationName);

        // Build the customizer
        return (builder) -> builder.withCacheConfiguration(SHORT_LIVED_CACHE, shortLivedCache)
                .withCacheConfiguration(LONG_LIVED_CACHE, longLivedCache);
    }

    /**
     * Create a JSON serializer for the redis cache values.
     * 
     * @return the serializer
     */
    private SerializationPair<Object> createJsonSerializer() {
        GenericJackson2JsonRedisSerializer jsonSerializer = new GenericJackson2JsonRedisSerializer(
                buildRedisObjectMapper());
        SerializationPair<Object> jsonSerializationPair = SerializationPair.fromSerializer(jsonSerializer);
        return jsonSerializationPair;
    }

    /**
     * Create a JSON object mapper specific to redis. This is necessary to use the
     * {@link ObjectMapper#findAndRegisterModules()} method to add support for new
     * java date/time objects such as {@link Instant}.
     * 
     * The
     * {@link ObjectMapper#activateDefaultTyping(com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator, DefaultTyping, As)}
     * method is just a copy/paste from what the
     * {@link GenericJackson2JsonRedisSerializer} class does when creating a default
     * object mapper. It's needed to add type information to the JSON so the code
     * knows how to deserialize it.
     * 
     * @return the object mapper
     */
    private ObjectMapper buildRedisObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.activateDefaultTyping(mapper.getPolymorphicTypeValidator(), DefaultTyping.EVERYTHING, As.PROPERTY);
        mapper.findAndRegisterModules();
        return mapper;
    }

    /**
     * Override the default cache error handler with our own implementation.
     */
    @Override
    public CacheErrorHandler errorHandler() {
        return new CustomCacheErrorHandler();
    }

}