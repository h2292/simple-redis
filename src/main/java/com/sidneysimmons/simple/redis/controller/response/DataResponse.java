package com.sidneysimmons.simple.redis.controller.response;

import com.sidneysimmons.simple.redis.service.domain.DataDto;

import lombok.Data;

/**
 * Data response.
 */
@Data
public class DataResponse {

    private DataDto data;
    private String returnedFromHost;

}
