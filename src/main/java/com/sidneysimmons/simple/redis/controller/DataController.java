package com.sidneysimmons.simple.redis.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sidneysimmons.simple.redis.controller.request.DataRequest;
import com.sidneysimmons.simple.redis.controller.response.DataResponse;
import com.sidneysimmons.simple.redis.service.DataService;
import com.sidneysimmons.simple.redis.service.domain.DataDto;
import com.sidneysimmons.simple.redis.util.HostUtil;

import jakarta.annotation.Resource;

/**
 * Controller for interacting with data.
 */
@RestController
@RequestMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
public class DataController {

    @Resource(name = "dataService")
    private DataService dataService;

    /**
     * Get short lived data by ID.
     * 
     * @param request the request
     * @return the response
     */
    @PostMapping(value = "/get-short-lived-data-by-id")
    public DataResponse getShortLivedDataById(@RequestBody DataRequest request) {
        DataDto data = dataService.getShortLivedData(request.getDataId());
        DataResponse response = new DataResponse();
        response.setData(data);
        response.setReturnedFromHost(HostUtil.getHostName());
        return response;
    }

    /**
     * Get long lived data by ID.
     * 
     * @param request the request
     * @return the response
     */
    @PostMapping(value = "/get-long-lived-data-by-id")
    public DataResponse getLongLivedDataById(@RequestBody DataRequest request) {
        DataDto data = dataService.getLongLivedData(request.getDataId());
        DataResponse response = new DataResponse();
        response.setData(data);
        response.setReturnedFromHost(HostUtil.getHostName());
        return response;
    }

    /**
     * Invalidate specific short lived data by ID.
     * 
     * @param request the request
     */
    @PostMapping(value = "/invalidate-short-lived-data-by-id")
    public void invalidateShortLivedData(@RequestBody DataRequest request) {
        dataService.invalidateShortLivedData(request.getDataId());
    }

    /**
     * Invalidate specific long lived data by ID.
     * 
     * @param request the request
     */
    @PostMapping(value = "/invalidate-long-lived-data-by-id")
    public void invalidateLongLivedData(@RequestBody DataRequest request) {
        dataService.invalidateLongLivedData(request.getDataId());
    }

    /**
     * Invalidate entire short lived cache.
     */
    @PostMapping(value = "/invalidate-short-lived-data")
    public void invalidateShortLivedData() {
        dataService.invalidateShortLivedData();
    }

    /**
     * Invalidate entire long lived cache.
     */
    @PostMapping(value = "/invalidate-long-lived-data")
    public void invalidateLongLivedData() {
        dataService.invalidateLongLivedData();
    }

}
