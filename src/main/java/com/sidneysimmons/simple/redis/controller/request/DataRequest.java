package com.sidneysimmons.simple.redis.controller.request;

import lombok.Data;

/**
 * Data request.
 */
@Data
public class DataRequest {

    private Long dataId;

}
