package com.sidneysimmons.simple.redis.service;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.sidneysimmons.simple.redis.configuration.CacheConfiguration;
import com.sidneysimmons.simple.redis.service.domain.DataDto;
import com.sidneysimmons.simple.redis.util.HostUtil;

/**
 * Data service.
 */
@Service("dataService")
public class DataService {

    @Autowired
    private CacheManager cacheManager;

    /**
     * Get short lived data for the given ID.
     * 
     * Note: This is cached.
     * 
     * @param dataId the data ID
     * @return the data if found, null otherwise
     */
    @Cacheable(value = CacheConfiguration.SHORT_LIVED_CACHE)
    public DataDto getShortLivedData(Long dataId) {
        // Wait for 5 seconds to illustrate an expensive operation
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // Nothing to do here because this is just for the example
        }

        // Create and return the data
        DataDto data = new DataDto();
        data.setDataId(dataId);
        data.setCachedOnHost(HostUtil.getHostName());
        data.setCachedTimestamp(Instant.now());
        return data;
    }

    /**
     * Get long lived data for the given ID.
     * 
     * Note: This is cached.
     * 
     * @param dataId the data ID
     * @return the data if found, null otherwise
     */
    @Cacheable(value = CacheConfiguration.LONG_LIVED_CACHE)
    public DataDto getLongLivedData(Long dataId) {
        // Wait for 5 seconds to illustrate an expensive operation
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // Nothing to do here because this is just for the example
        }

        // Create and return the data
        DataDto data = new DataDto();
        data.setDataId(dataId);
        data.setCachedOnHost(HostUtil.getHostName());
        data.setCachedTimestamp(Instant.now());
        return data;
    }

    /**
     * Invalidate short lived data for a specific ID.
     * 
     * @param dataId the data ID
     */
    public void invalidateShortLivedData(Long dataId) {
        cacheManager.getCache(CacheConfiguration.SHORT_LIVED_CACHE).evict(dataId);
    }

    /**
     * Invalidate long lived data for a specific ID.
     * 
     * @param dataId the data ID
     */
    public void invalidateLongLivedData(Long dataId) {
        cacheManager.getCache(CacheConfiguration.LONG_LIVED_CACHE).evict(dataId);
    }

    /**
     * Invalidate the entire short lived cache.
     */
    public void invalidateShortLivedData() {
        cacheManager.getCache(CacheConfiguration.SHORT_LIVED_CACHE).clear();
    }

    /**
     * Invalidate the entire long lived cache.
     */
    public void invalidateLongLivedData() {
        cacheManager.getCache(CacheConfiguration.LONG_LIVED_CACHE).clear();
    }

}
