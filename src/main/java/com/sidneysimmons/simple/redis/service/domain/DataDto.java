package com.sidneysimmons.simple.redis.service.domain;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Data DTO.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataDto {

    private Long dataId;
    private String cachedOnHost;
    private Instant cachedTimestamp;

}
