package com.sidneysimmons.simple.redis.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import lombok.extern.slf4j.Slf4j;

/**
 * Utility methods related to the host.
 */
@Slf4j
public class HostUtil {

    private HostUtil() {
        // This class is just for static methods
    }

    /**
     * Get the host name.
     * 
     * @return the host name if it can be seen, null otherwise
     */
    public static String getHostName() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            log.error("Cannot get the host name.", e);
        }
        return hostName;
    }

}
