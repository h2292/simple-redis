# Java base image
FROM eclipse-temurin:17.0.5_8-jdk-alpine

# Ports the application uses
EXPOSE 8080 8000

# The application's jar file
ARG JAR_FILE=build/libs/simple-redis.jar

# Add the jar to the container
COPY ${JAR_FILE} simple-redis.jar

# Run the application
# ENTRYPOINT ["tail", "-f", "/dev/null"]
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,address=*:8000,server=y,suspend=n", "-jar", "/simple-redis.jar"]